<?php

// The data to send to the API
$postData = array(
    "image" => "uploads/image-1533634074708source_2.jpg",
    "redactorWidth" => "700",
    "redactorHeight" => "408",
    "appWidth" => "50",
    "appHeight" => "25",
    "appBorderWidth" => "5",
    "appBorderOrWidth" => "5",
    "multiplier" => "1.8188976377952755",
    "appDPI" => "53.9",
    "margin" => "2.5",
    "appBorderType" => "mirror",
    "text" => array("value" => "Test"),
    "logo" => array("source" => "Amarok-logo-small.png"),
    "left" => "53",
    "top" => "-324",
    "scaleX" => "0.55",
    "scaleY" => "0.55",
    "filters" => array()
);


$data_string = json_encode($postData);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost:3000/api");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type:application/json',
    'Content-Length: ' . strlen($data_string)
));
$output = curl_exec($ch);
if ($output === FALSE) {
    die(curl_error($ch));
}
var_dump($output)  ;
curl_close($ch); 
