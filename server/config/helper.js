const fs = require('fs'),
    fabric = require('fabric').fabric,
    gm = require('gm').subClass({imageMagick: true}),
    piexif = require("piexifjs");
const PIXEL_SIZE = 255,
    IMAGE_SETTINGS = {
        FILE_FORMAT: '.jpeg',
        OUTPUT: 'output',
        DPI_VALUE: 300,
        DPI_DELTA: 2.54,
        DEFAULT_MARGIN: 2.5,//in cm
        OR_W_REQ_W: 1,//relation btw origin width and width from request
        BORDER_TYPE: {
            MIRROR: 'mirror',
            EXPANDET: 'expandet'
        },
        POSITION: {
            CENTER: 'center',
            LEFT: 'left',
            TOP: 'top',
            BOTTOM: 'bottom',
            RIGHT: 'right',
        }
    };


function applyFilter(reqdata, el, canvas) {
    return new Promise((resolve) => {
        var file_name = uuid() + IMAGE_SETTINGS.FILE_FORMAT;
        var out = fs.createWriteStream(`${IMAGE_SETTINGS.OUTPUT}/temp` + file_name);
        saveImage(out, canvas._objects[0], reqdata, canvas).then(() => {
            fabric.Image.fromURL(out.path, function (oImg) {
                fs.unlinkSync(out.path);
                var _canvas = new fabric.Canvas(null, {
                    width: oImg.width,
                    height: oImg.height
                });
                oImg.set({
                    scaleX: 1,
                    scaleY: 1,
                    left: 0,
                    top: 0,
                }).scale(1);
                _canvas.add(oImg);
                _canvas.renderAll();

                _applyFilters(oImg, _canvas, reqdata);
                resolve(_canvas);
            })
        })

    })
}

function addResolution(inputBuffer, resizeWidth, resizeHeight, reqData) {
    return new Promise((resolve, reject) => {

        gm(inputBuffer)
            .units('PixelsPerInch')
            .density(reqData.appDPI)
            // .resample(reqData.imageOrWidth, reqData.imageOrHeight) // resampled to 150 resolution
            // you have to set the width and height again because resample rearranges those params
            // .resize(reqData.imageOrWidth, reqData.imageOrHeight, '!')
            // .resize(resizeWidth, resizeHeight, '!')
            .toBuffer('JPEG', function (err, buffer) {
                // if (err) reject(err)
                // resolve(buffer)
                fs.writeFile(inputBuffer, buffer, function (err) {
                    if (err) {
                        return console.log("Error------", err);
                    }

                    resolve();
                });
            })
    })
}

function _applyFilters(el, canvas, reqdata) {
    var f = fabric.Image.filters;
    el.filters = [];
    for (let i = 0; i < reqdata.filters.length; i++) {
        let _filter = reqdata.filters[i];
        switch (_filter.type.toLowerCase()) {
            case 'sepia':
                el.filters.push(new f.Sepia());
                break;
            case 'vintage':
                el.filters.push(new f.Vintage());
                break;
            case 'technicolor':
                el.filters.push(new f.Technicolor());
                break;
            case 'blackwhite':
                el.filters.push(new f.BlackWhite());
                break;
            case 'kodachrome':
                el.filters.push(new f.Kodachrome());
                break;
            case 'polaroid':
                el.filters.push(new f.Polaroid());
                break;
            case 'brownie':
                el.filters.push(new f.Brownie());
                break;
            case 'grayscale':
                el.filters.push(new f.Grayscale());
                break;
            case 'blur':
                el.filters.push(new f.Blur(70));
                break;
            case 'pixelate':
                el.filters.push(new f.Pixelate(70));
                break;
            case 'invert':
                el.filters.push(new f.Invert());
        }
    }

    el.applyFilters();
    canvas.renderAll();
}

function transformImage(reqData) {
    return new Promise((reslove) => {
        fabric.Image.fromURL(reqData.imagePath, function (orImg) {
            var isImageBigger =   orImg.width > reqData.appWidth,
                _deltaBorder = 0,//=reqData.imageBorderType==IMAGE_SETTINGS.BORDER_TYPE.EXPANDET?0:reqData.appBorderOrWidth*2,
                _w = isImageBigger ? orImg.width : reqData.appWidth,
                _h = isImageBigger ? Math.round(_w * (reqData.appHeight / reqData.appWidth)) : reqData.appHeight,
                _aspectAppWidthOrWidth = _w / reqData.appWidth,
                canvasOrigin = new fabric.Canvas(null, {
                    width: _w,
                    height: _h,
                    backgroundColor: '#0ff0ff'
                });

            reqData._or_margin = reqData.margin;
            reqData._aspectAppWidthOrWidth = isImageBigger ? _aspectAppWidthOrWidth : 1;
            reqData.margin = Math.round(reqData.margin * reqData._aspectAppWidthOrWidth);
            reqData._appWidth = Math.round(reqData._aspectAppWidthOrWidth*reqData.appWidth);
            // console.log(reqData._or_margin,reqData.margin);
            orImg.set({
                scaleX: (reqData.imageScaleX  ) * _aspectAppWidthOrWidth,
                scaleY: (reqData.imageScaleY  ) * _aspectAppWidthOrWidth,
                left: ((reqData.imageOffsetX   ) * _aspectAppWidthOrWidth)  ,
                top: ((reqData.imageOffsetY  ) * _aspectAppWidthOrWidth) ,
            });
            canvasOrigin.add(orImg);
            canvasOrigin.renderAll();
            reslove({canvas: canvasOrigin, oImg: orImg});
        })
    })
}


/*function transformImage(reqData) {
    return new Promise((reslove) => {
        fabric.Image.fromURL(reqData.imagePath, function (orImg) {
            var canvasOrigin = new fabric.Canvas(null, {
                width: reqData.appWidth ,
                height: reqData.appHeight ,
                backgroundColor: '#0ff0ff'

            });

            orImg.set({
                scaleX: reqData.imageScaleX,
                scaleY: reqData.imageScaleY,
                left: (reqData.imageOffsetX),
                top: (reqData.imageOffsetY),
            });
            reqData._or_margin = reqData.margin;
            reqData._aspectAppWidthOrWidth =  1;
            reqData._appWidth = reqData._aspectAppWidthOrWidth*reqData.appWidth;
            canvasOrigin.add(orImg);
            canvasOrigin.renderAll();
            // applyBorder(, reqData).then((opt) => {
            reslove({canvas:canvasOrigin,oImg:orImg});
            // });

        })
    })
}*/
function applyBorderType(cnvs, reqData) {
    switch (reqData.imageBorderType) {
        case IMAGE_SETTINGS.BORDER_TYPE.EXPANDET: {
            //no need to do anything
            break
        }
        case IMAGE_SETTINGS.BORDER_TYPE.MIRROR: {
            flipImageBorders({canvas: cnvs, reqData});
            break
        }
        default: {
            var colorInHex = reqData.imageBorderType,
                _margin = Math.round(reqData.appBorderOrWidth * reqData._aspectAppWidthOrWidth),
                isHex = colorInHex && colorInHex.match("#"),
                Rx = isHex ? hexToR(reqData.imageBorderType) : PIXEL_SIZE,
                Gx = isHex ? hexToG(reqData.imageBorderType) : PIXEL_SIZE,
                Bx = isHex ? hexToB(reqData.imageBorderType) : PIXEL_SIZE,
                _imageData = borderPixels(cnvs, reqData, 0, _margin),
                ctx = cnvs.getContext('2d');

            for (let di = 0; di < _imageData.length; di++) {
                let data = _imageData[di]._d.data;
                for (var i = 0; i < data.length; i += 4) {
                    data[i + 0] = Rx;
                    data[i + 1] = Gx;
                    data[i + 2] = Bx;
                }
                updateImage(di, ctx, _imageData[di]._d, reqData, cnvs, 0, _margin);
            }

            break
        }
    }
}

function borderPixels(canvas, reqData, margin = 0, _borderWidth = 0) {
    var ctx = canvas.getContext('2d'),
        borderWidth = _borderWidth;//Math.ceil(reqData.aspectBorderWidthToAppwidth * canvas.width);
    return [
        {
            _d: ctx.getImageData(margin, margin, canvas.width - 2 * margin, borderWidth), _hor: true//top
        },
        {
            _d: ctx.getImageData(margin, canvas.height - margin - borderWidth, canvas.width - 2 * margin, borderWidth),
            _hor: true//bottom
        },
        {
            _d: ctx.getImageData(margin, margin, borderWidth, canvas.height - 2 * margin)//left
        },
        {
            _d: ctx.getImageData(canvas.width - margin - borderWidth, margin, borderWidth, canvas.height - 2 * margin)//right
        }
    ]
}

function updateImage(i, ctx, _data, reqData, canvas, margin = 0, _borderWidth = 0) {
    let borderWidth = _borderWidth;//Math.ceil(reqData.aspectBorderWidthToAppwidth * canvas.width);
    if (i == 0) {//top
        ctx.putImageData(_data, margin, margin);
    } else if (i == 1) {//bottom
        ctx.putImageData(_data, margin, canvas.height - margin - borderWidth);
    } else if (i == 2) {//left
        ctx.putImageData(_data, margin, margin);
    } else if (i == 3) {//right
        ctx.putImageData(_data, canvas.width - margin - borderWidth, margin);
    }
}

function flipImageBorders(opt) {
    let {canvas, reqData} = opt,
        ctx = canvas.getContext('2d'),
        margin = Math.round(reqData.appBorderOrWidth * reqData._aspectAppWidthOrWidth),//Math.ceil(reqData.aspectBorderWidthToAppwidth * canvas.width),
        marginVer = margin,
        _imageData = [
            {
                x: margin, y: margin, width: margin, height: canvas.height - 2 * margin, out_x: 0, out_y: margin//left
            },
            {
                x: canvas.width - 2 * margin,
                y: margin,
                width: margin,
                height: canvas.height - 2 * margin,
                out_x: canvas.width - margin,
                out_y: margin//right
            },
            {
                x: 0, y: marginVer, width: canvas.width, height: marginVer, out_x: 0, out_y: 0, hor: true//top
            },
            {
                x: 0,
                y: canvas.height - 2 * marginVer,
                width: canvas.width,
                height: marginVer,
                out_x: 0,
                out_y: canvas.height - marginVer,
                hor: true//bottom
            }
        ];

    for (let i = 0; i < _imageData.length; i++) {
        let _ddat = _imageData[i],
            _data = flip_image(ctx.getImageData(_ddat.x, _ddat.y, _ddat.width, _ddat.height), i > 1);
        ctx.putImageData(_data, _ddat.out_x, _ddat.out_y);
    }

}

function flip_image(imgData, isHorizontal) {


    var data = imgData.data;
    if (isHorizontal) {
        for (var y = 0; y < imgData.height / 2; y++) {
            let _op = imgData.height - 1 - y;
            for (var x = 0; x < imgData.width; x++) {

                var pixel = (y * imgData.width + x) * 4,
                    opositePixel = (_op * imgData.width + x) * 4,
                    _oldR = data[pixel],
                    _oldG = data[pixel + 1],
                    _oldB = data[pixel + 2]
                ;
                data[pixel] = data[opositePixel];
                data[pixel + 1] = data[opositePixel + 1];
                data[pixel + 2] = data[opositePixel + 2];

                data[opositePixel] = _oldR;
                data[opositePixel + 1] = _oldG;
                data[opositePixel + 2] = _oldB;
            }
        }
    } else {
        for (var y = 0; y < imgData.height; y++) {
            for (var x = 0; x < imgData.width / 2; x++) {

                var pixel = (y * imgData.width + x) * 4,
                    opositePixel = (y * imgData.width + (imgData.width - 1 - x)) * 4,
                    _oldR = data[pixel],
                    _oldG = data[pixel + 1],
                    _oldB = data[pixel + 2]
                ;
                data[pixel] = data[opositePixel];
                data[pixel + 1] = data[opositePixel + 1];
                data[pixel + 2] = data[opositePixel + 2];

                data[opositePixel] = _oldR;
                data[opositePixel + 1] = _oldG;
                data[opositePixel + 2] = _oldB;
            }
        }
    }


    return imgData;
}

function multiplyier(reqData,width){
    return (reqData.appDPI * width) / 2.54 / (reqData.appWidth*reqData._aspectAppWidthOrWidth);
}
function manageImage1(canvs, reqData) {
    return new Promise((resolve) => {
        var file_name = uuid() + IMAGE_SETTINGS.FILE_FORMAT;
        var out = fs.createWriteStream(`${IMAGE_SETTINGS.OUTPUT}/temp` + file_name);
        saveImage(out, canvs._objects[0], reqData, canvs).then(() => {
            fabric.Image.fromURL(out.path, function (oImg) {
                fs.unlinkSync(out.path);
                var scale = 1,//(reqData.imageWidth) / oImg.width,
                    margin =  reqData._or_margin,
                    borderWidth = reqData.appBorderOrWidth,
                    _wS = Math.round(reqData.appWidth  ),// * reqData.multiplier,//Math.round(oImg.width* reqData.multiplier) ,//  resize to smaller -> will lose the quality
                    multiplyer =  reqData.multiplier,//multiplyier(reqData,reqData.imageOrWidth+2*reqData.imageOrBorderWidth ),
                    _w = Math.round(reqData.appWidth + reqData.appBorderOrWidth * 2) * reqData.multiplier,
                    _h = Math.round(_w * (reqData.appHeight / reqData.appWidth)),//Math.round(reqData.appHeight + reqData.appBorderOrWidth * 2) * reqData.multiplier,//Math.round(oImg.height* reqData.multiplier),
                    canvas = new fabric.Canvas(null, {
                        width: _w + (margin * 2),
                        height: _h + (margin * 2),
                        // backgroundColor: '#ff0000'
                    });
                console.log(multiplyer,reqData.multiplier);
                scale = (_w ) / oImg.width;
                oImg.set({
                    scaleX: scale,
                    scaleY: scale,
                    left: (margin),
                    top: (margin),
                });
                // console.log(scale);
                canvas.add(oImg);
                oImg.center();
                canvas.renderAll();
                // reqData.imageWidth = _w;
                checkBorders(reqData, canvas).then((_canvas) => {
                    let _c = _canvas.canvas;
                    // let _c = canvas;
                    addLogo(reqData, _c).then(() => {
                        addText(reqData, _c).then(() => {
                            addCornerMarkers(_c, reqData.margin);
                            resolve({oImg: _c._objects[0], canvas: _c});
                        });
                    });
                });


            });
        });

    })
}

function manageImage(canvs, reqData) {
    return new Promise((resolve) => {
        var file_name = uuid() + IMAGE_SETTINGS.FILE_FORMAT;
        var out = fs.createWriteStream(`${IMAGE_SETTINGS.OUTPUT}/temp` + file_name);
        saveImage(out, canvs._objects[0], reqData, canvs).then(() => {
            fabric.Image.fromURL(out.path, function (oImg) {
                fs.unlinkSync(out.path);
                var scale = 1,//(reqData.imageWidth) / oImg.width,
                    margin = reqData.margin,
                    canvas = new fabric.Canvas(null, {
                        width: oImg.width + (margin * 2),
                        height: oImg.height + (margin * 2),
                        backgroundColor: '#ffffff'
                    });
                oImg.set({
                    scaleX: scale,
                    scaleY: scale,
                    left: (margin),
                    top: (margin),
                });
                canvas.add(oImg);
                canvas.renderAll();

                checkBorders(reqData, canvas).then((_canvas) => {
                    let _c = _canvas.canvas;
                    addLogo(reqData, _c).then(() => {
                        addText(reqData, _c).then(() => {
                            addCornerMarkers(_c, reqData.margin);
                            // applyBorder({oImg: _c._objects[0], canvas: _c},reqData).then((options)=>{
                            saveT({oImg: _c._objects[0], canvas: _c},reqData).then((opt)=>{
                                resolve(opt);
                            });
                            // })

                        });
                    });
                });


            });
        });

    })
}

function saveT(_opt,reqData){
    return new Promise((resolve => {
        var file_name = uuid() + '.jpeg';
        var out = fs.createWriteStream(`${IMAGE_SETTINGS.OUTPUT}/temp` + file_name);

        saveImage(out, _opt.oImg, reqData, _opt.canvas).then(() => {
            fabric.Image.fromURL(out.path, function (oImg) {
                fs.unlinkSync(out.path);
                reqData.multiplier = multiplyier(reqData,reqData.imageOrWidth+2*reqData.imageOrBorderWidth );
                let canvas = new fabric.Canvas(null, {
                    width: Math.round(oImg.width * reqData.multiplier),
                    height: Math.round(oImg.height * reqData.multiplier),
                    backgroundColor: '#ffffff'
                });
                canvas.add(oImg);
                oImg.scale(reqData.multiplier);
                oImg.center();
                canvas.renderAll();

                resolve({oImg,canvas});
            })

        });
    }))

}
function addCornerMarkers(_c, width) {
    let cntx = _c.getContext('2d'),
        lines = [
            [width, 0, 0, 0, 0, width],//left top
            [_c.width - width, 0, _c.width, 0, _c.width, width],//right top
            [0, _c.height - width, 0, _c.height, width, _c.height],//left bottom
            [_c.width - width, _c.height, _c.width, _c.height, _c.width, _c.height - width]//right bottom
        ];
    cntx.strokeStyle = "#ff0000";
    cntx.lineWidth = 2;
    lines.forEach((line) => {
        cntx.beginPath();
        cntx.moveTo(line[0], line[1]);
        cntx.lineTo(line[2], line[3]);
        cntx.lineTo(line[4], line[5]);
        cntx.stroke();
    })
}

function checkBorders(reqData, canvas) {


    return new Promise(((resolve) => {
        if (reqData.imageBorderWidth > 0) {
            let context = canvas.getContext('2d');
            context.fillStyle = "#ffffff";
            [
                {x: 0, y: 0, width: canvas.width, height: reqData.margin},
                {
                    x: 0,
                    y: canvas.height - reqData.margin,
                    width: canvas.width,
                    height: reqData.margin
                },
                {x: 0, y: 0, width: reqData.margin, height: canvas.height},
                {
                    x: canvas.width - reqData.margin,
                    y: 0,
                    width: reqData.margin,
                    height: canvas.height
                }
            ].forEach((el) => {
                context.fillRect(el.x, el.y, el.width, el.height);
            });
            var file_name = uuid() + IMAGE_SETTINGS.FILE_FORMAT;
            var out = fs.createWriteStream(`${IMAGE_SETTINGS.OUTPUT}/temp` + file_name);
            saveImage(out, canvas._objects[0], reqData, canvas).then(() => {
                fabric.Image.fromURL(out.path, function (oImg) {
                    fs.unlinkSync(out.path);
                    var _canvas = new fabric.Canvas(null, {
                        width: oImg.width,
                        height: oImg.height,
                        backgroundColor: '#ffffff'
                    });
                    oImg.set({
                        scaleX: 1,
                        scaleY: 1,
                        left: 0,
                        top: 0
                    });
                    _canvas.add(oImg);
                    _canvas.renderAll();
                    resolve({canvas: _canvas});
                });
            });
        } else {
            resolve({canvas});
        }

    }))
}

function saveImage(stream, oImg, reqData, canvas) {
    var out = stream;

    return new Promise((resolve) => {
        var stream = canvas.createJPEGStream({
            quality: 100
        });
        stream.on('data', function (chunk) {
            out.write(chunk);
        });

        stream.on('end', function () {
            out.end();
        });

        out.on('close', function () {
            var jpeg = fs.readFileSync(out.path);
            var data = jpeg.toString("binary");
            var meta = {};
            var dpi_calc = Number((oImg.width / (oImg.width * oImg.scaleX / canvas.width) / (reqData.imageWidth * 0.393701)).toFixed(1));
            meta[piexif.ImageIFD.XResolution] = [dpi_calc, 1];
            meta[piexif.ImageIFD.YResolution] = [dpi_calc, 1];
            // console.log(dpi_calc);
            meta[piexif.ImageIFD.Software] = "Andzej";
            var exifObj = {
                "0th": meta
            };
            var exifbytes = piexif.dump(exifObj);
            var newData = piexif.insert(exifbytes, data);
            var newJpeg = new Buffer(newData, "binary");
            fs.writeFileSync(out.path, newJpeg);
            // output
            resolve()

        });
    })
}

function convertToPixells(value, type = 1) {
    switch (type) {
        case 1: {//cm -> pixel
            return Math.ceil(value * IMAGE_SETTINGS.DPI_DELTA);
        }
    }
}

function cmToPx(value) {
    return Math.round(value * IMAGE_SETTINGS.OR_W_REQ_W);
}

function cutHex(h) {
    return (h.charAt(0) == "#") ? h.substring(1, 7) : h
};

function hexToR(h) {
    return parseInt((cutHex(h)).substring(0, 2), 16)
};

function hexToG(h) {
    return parseInt((cutHex(h)).substring(2, 4), 16)
};

function hexToB(h) {
    return parseInt((cutHex(h)).substring(4, 6), 16)
};


function addLogo(reqData, canvas) {
    return new Promise((reslove) => {
        if (reqData.logo !== false) {
            fabric.Image.fromURL(reqData.logo.source, function (logo) {
                if (!logo || logo.getElement() === undefined) return reslove();
                let _height = Math.abs(isNumber(reqData.logo.height, cmToPx(reqData.margin)));
                // let templates = new fabric.Group([logo],{height:reqData.margin});
                var _canvas = new fabric.Canvas(null, {
                    width: _height * logo.width / logo.height,
                    height: _height,
                    // backgroundColor:"#ff0000"
                });
                _canvas.add(logo);
                // logo.set({
                //     originX:'center',
                //     originY:'center'
                // })
                // templates.height = reqData.margin;
                logo.scaleToHeight(_height);
                // logo.set({
                //     left: canvas.width / 2 - logo.width / 2,
                //     top: 0
                // templates.add(logo);


                logo.center();
                // canvas.add(logo);
                // logo.center();
                // templates.top = 0;
                // logo.top = 0;
                // let _l = logo.getBoundingRect();
                // logo.top =  reqData.margin - _l.height/2 ;
                // console.log(logo.top, _l,reqData.margin);
                // });

                // logo.top = reqData.margin/2;// - (logo.height/2)*logo.scaleY;
                // console.log(logo.top,logo.scaleY,logo.height,reqData.margin,_l);
                _canvas.renderAll();
                saveBase64(_canvas.toDataURL('image/png')).then((path) => {
                    fabric.Image.fromURL(path, function (oImg) {
                        fs.unlinkSync(path);
                        canvas.add(oImg);
                        oImg.center();
                        oImg.top = reqData.margin / 2 - oImg.height / 2;
                        // console.log(oImg.height,oImg.top,reqData.margin );
                        canvas.renderAll();
                        reslove();
                    });
                })

            });
        } else {
            reslove();
        }
    })

}

function isNumber(value, defaultV) {
    return typeof value != 'undefined' && !isNaN(parseFloat(value)) && value != Infinity && value != -Infinity ? parseFloat(value) : defaultV;
}

function addText(reqData, canvas) {
    return new Promise((resolve) => {
        if (reqData.text !== false) {
            var textSize = Math.abs(isNumber(reqData.text.size, 16)),
                text = new fabric.Text(
                    reqData.text.value,
                    {
                        fontSize: textSize
                    }
                ),
                _canvas = new fabric.Canvas(null, {
                    width: text.width,
                    height: text.height,
                });
            _canvas.add(text);
            text.center();
            saveBase64(_canvas.toDataURL('image/png')).then((path) => {
                fabric.Image.fromURL(path, function (oImg) {
                    fs.unlinkSync(path);
                    let scale = reqData.margin / text.height;
                    // text.scaleToHeight( 2*(reqData.margin));
                    // text.set({originX: 'center', originY: 'center'});
                    // oImg.scale(scale);
                    canvas.add(oImg);
                    oImg.center();
                    let _l = oImg.getBoundingRect();
                    // oImg.top = _l.top * 2 ;//- oImg.height/2;
                    oImg.top = canvas.height - reqData.margin / 2 - oImg.height / 2;
                    canvas.renderAll();
                    resolve()
                });
            })


        } else {
            resolve();
        }
    })

}

function saveBase64(data, fileName) {
    return new Promise((resolve) => {
        var base64Data = data.replace(/^data:image\/png;base64,/, "");
        // let base64Data = data.split(';base64,').pop();
        var imageName = fileName || "temp_out.png";
        fs.writeFile(imageName, base64Data, 'base64', function (err) {
            resolve(imageName);
        });
    });

}

function uuid(len = 45, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    charSet += Date.now().toString(32);
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
}

module.exports = {
    PIXEL_SIZE,
    IMAGE_SETTINGS,
    addResolution,
    utilTransformedBorderWidth: cmToPx,
    transformImage, applyFilter, _applyFilters, applyBorderType, manageImage, saveImage, convertToPixells, saveBase64,
    uuid
}