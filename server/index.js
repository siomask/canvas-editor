var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    HELPER = require('./config/helper'),
    {IMAGE_SETTINGS} = HELPER,
    path = require("path");
;

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');//allow request from anywhere, not safe, replace * with your domain name
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(express.static('public'));
app.use(`/${IMAGE_SETTINGS.OUTPUT}`, express.static(`${IMAGE_SETTINGS.OUTPUT}`));

app.use("/", require("./routes/index"));

// app.use("*", function (req, res) {
//     res.sendFile(path.join(__dirname, "../public/dist/index.html"));
// });


var server = app.listen(3000, function () {
    console.log("app running on port.", server.address().port,server.address());
});