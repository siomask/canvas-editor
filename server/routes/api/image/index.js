const router = require("express").Router();
var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/')
    },
    filename: (req, file, cb) => {
        file._tempName = file.fieldname + '-' + Date.now() + file.originalname;
        cb(null, file._tempName)
    }
});
var upload = multer({storage: storage});



router.post('/', upload.single('image'), function (req, res) {
    res.send('uploads/' + req.file._tempName);
});

router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about image api"
    });
});
module.exports = router;