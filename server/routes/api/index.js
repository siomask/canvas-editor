const router = require("express").Router(),
    fs = require('fs'),
    fabric = require('fabric').fabric,
    HELPER = require('../../config/helper'),
    {
        IMAGE_SETTINGS,
        transformImage, applyFilter, _applyFilters, applyBorderType, manageImage, saveImage, convertToPixells, addResolution,saveBase64,
        uuid,
    } = HELPER;

router.use('/image', require("./image/index.js"));


router.get("", function (req, res) {
    return res.json({
        status: true,
        message: 'Andzej get request'
    });
});
router.post('', function (req, res) {
    try {
        generateImage(req, res);
    } catch (e) {
        res.send({error:"something bad",data:e});
    }
});

function generateImage(req, res) {
    var
        _body =  (req.body),
        appDPI = IMAGE_SETTINGS.DPI_DELTA = parseFloat(_body.appDPI || 1),
        reqData = {
            imagePath: _body.image ||"source.jpg",
            appWidth: (Math.abs(_body.redactorWidth || 1)),//with of area where image was setted
            appHeight: (Math.abs(_body.redactorHeight || 1)),//height of area where image was setted
            imageOrWidth: (Math.abs(_body.appWidth || 1)),//origin output canvas width
            imageOrHeight: (Math.abs(_body.appHeight || 1)),//origin output canvas height
            imageWidth: convertToPixells(Math.abs(_body.appWidth || 1)),//output canvas width
            imageHeight: convertToPixells(Math.abs(_body.appHeight || 1)),//output canvas height
            imageBorderWidth: convertToPixells(Math.abs(_body.appBorderWidth || 1)),
            imageOrBorderWidth: parseFloat(_body.appBorderWidth||1),
            appBorderOrWidth: parseFloat(_body.appBorderWidth||1),//origin border width in area where image was setted
            imageBorderType: _body.appBorderType || IMAGE_SETTINGS.BORDER_TYPE.EXPANDET,
            appDPI: appDPI,
            multiplier: parseFloat(_body.multiplier || 1),
            imageScaleX: parseFloat(_body.scaleX || 1),
            imageScaleY: parseFloat(_body.scaleY || 1),
            imageOffsetX: parseFloat((_body.left || 0)),//in pixels
            imageOffsetY: parseFloat((_body.top || 0)),//in pixels
            filters: _body.filters ? (typeof _body.filters == 'string' ? JSON.parse(_body.filters) : _body.filters) : [],
            getImage: _body.getImage || 'source.jpg',
            saveImage: _body.saveImage,
            margin: (Math.abs(_body.margin === undefined ? IMAGE_SETTINGS.DEFAULT_MARGIN : (isNaN(parseFloat(_body.margin)) ? IMAGE_SETTINGS.DEFAULT_MARGIN : parseFloat(_body.margin)))),
            logo: !_body.logo || !_body.logo.source ? {
                source: 'Amarok-logo-small.png'
            } : _body.logo,
            text: !_body.text || !_body.text.value ? {
                value: 'hahahahah'
            } : _body.text
        };
    reqData.aspectBorderWidthToAppwidth = reqData.imageOrBorderWidth / reqData.imageOrWidth;
    reqData.aspectBorderHeightToAppheight = reqData.imageOrBorderWidth / reqData.imageOrHeight;
    reqData.imageWidth += reqData.imageBorderWidth * 2;
    reqData.imageHeight += reqData.imageBorderWidth * 2;
    // reqData.imageWidth = reqData.imageOrWidth +2*reqData.imageOrBorderWidth;//+2*reqData.margin;
    // console.log(    _body);
    var appWidth = reqData.imageOrWidth + 2 * reqData.appBorderOrWidth;
    reqData.margin = Math.round(reqData.margin * (reqData.appWidth / appWidth));
    reqData.appBorderOrWidth = Math.round(reqData.appBorderOrWidth * (reqData.appWidth / appWidth));


    transformImage(reqData).then((optTransfrom) => {
        // var file_name = uuid() + '.jpeg';
        // var _outPath = `${IMAGE_SETTINGS.OUTPUT}/` + file_name;
        //
        // var _out = fs.createWriteStream(_outPath);
        // return saveImage(_out, optTransfrom.oImg, reqData, optTransfrom.canvas).then(() => {
        //     res.json({path:_outPath});
        // });
        let canvs = optTransfrom.canvas;
        if (reqData.imageBorderType != IMAGE_SETTINGS.BORDER_TYPE.MIRROR &&
            reqData.imageBorderType != IMAGE_SETTINGS.BORDER_TYPE.EXPANDET) {
            //apply filter before apply border type, so all pixelds will be correct
            _applyFilters(optTransfrom.oImg, optTransfrom.canvas, reqData);
        }

        applyBorderType(canvs, reqData);

        let _manageImage = (_c) => {

            manageImage(_c, reqData).then((_opt) => {
                var file_name = uuid() + '.jpeg';
                var _outPath = `${IMAGE_SETTINGS.OUTPUT}/` + file_name;

                var _out = fs.createWriteStream(_outPath);
                saveImage(_out, _opt.oImg, reqData, _opt.canvas).then(() => {
                    addResolution(_outPath, _opt.canvas.width, _opt.canvas.height, reqData).then(() => {
                        res.json({path:_outPath});
                    })
                });
            })
        }

        if (reqData.imageBorderType == IMAGE_SETTINGS.BORDER_TYPE.MIRROR ||
            reqData.imageBorderType == IMAGE_SETTINGS.BORDER_TYPE.EXPANDET) {
            applyFilter(reqData, null, canvs).then((_canvas) => {
                _manageImage(_canvas);
            })
        } else {
            _manageImage(canvs);
        }


    })
}


router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        data: req.user
    });
});
module.exports = router;
