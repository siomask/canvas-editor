function bind(scope, fn) {
    return function () {
        return fn.apply(scope, arguments);
    }
};

function hexToR(h) {
    return parseInt((cutHex(h)).substring(0, 2), 16)
};

function hexToG(h) {
    return parseInt((cutHex(h)).substring(2, 4), 16)
};

function hexToB(h) {
    return parseInt((cutHex(h)).substring(4, 6), 16)
};

function cutHex(h) {
    return (h.charAt(0) == "#") ? h.substring(1, 7) : h
};

function randomNumb(x, y) {
    return Math.floor(Math.random() * x) + y;
};

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

module.exports={
    isMobile,
    cutHex,
    hexToB,
    randomNumb,
    hexToR,
    bind,
    hexToG,
}