var editor = require('fabric').fabric;
const {
    isMobile,
    cutHex,
    hexToB,
    randomNumb,
    hexToR,
    bind,
    hexToG
} = require('./helpers'),
  config = require('./config'),
    {  _SETTINGS,_URL} = config;

editor.Canvas.prototype._SETTINGS = _SETTINGS;
editor.Canvas.prototype.__config = config;
editor.Canvas.prototype.preserveObjectStacking = true;
editor.Canvas.prototype.renderOnAddRemove = false;
editor.Canvas.prototype.selection = false;
editor.Canvas.prototype.allowTouchScrolling = true;
editor.Canvas.prototype.enableRetinaScaling = false;
editor.Canvas.prototype.skipOffscreen = false;
editor.Canvas.prototype.appWidth = 0;
editor.Canvas.prototype.appHeight = 0;
editor.Canvas.prototype.appRelativeWidth = 0;
editor.Canvas.prototype.appRelativeHeight = 0;
editor.Canvas.prototype.appBorderWidth = 2;
editor.Canvas.prototype.appBorderType = "expandet";
editor.Canvas.prototype.appRelativeZoom = 0;
editor.Canvas.prototype.appRender = false;
editor.Canvas.prototype.appParentContainer = "";


editor.Canvas.prototype.setImage = function (url, callback) {
    this.clear();
    var self = this;
    editor.Image.fromURL(url, function (image) {
        // tut callback
        image.set({
            lockRotation: true,
            centeredScaling: true,
            hasBorders: false,
            hasControls: false
        });
        self.add(image);
        self.requestRenderAll();
        self.setActiveObject(image);
        self.utilImageSize();
        if (callback != undefined) {
            callback()
        }
        ;
    });
};

editor.Canvas.prototype.setCanvas = function (w, h, u) {
    if (u == "mm" || u == "MM" || u == "Mm") {
        this.appWidth = (w / 10) + (this.appBorderWidth * 2);
        this.appHeight = (h / 10) + (this.appBorderWidth * 2);
    } else if (u == "cm" || u == "CM" || u == "Cm") {
        this.appWidth = w + (this.appBorderWidth * 2);
        this.appHeight = h + (this.appBorderWidth * 2);
    } else {
        this.appWidth = w + (this.appBorderWidth * 2);
        this.appHeight = h + (this.appBorderWidth * 2);
    }
    ;
    this.appRelativeWidth = w;
    this.appRelativeHeight = h;
    window.addEventListener("load", bind(this, this.utilResize), false);
    window.addEventListener("resize", bind(this, this.utilResize), false);
    this.utilResize();
    this.requestRenderAll();
};

editor.Canvas.prototype.startEvents = function () {
    var self = this;
    this.on("object:moving", function (e) {
        self.utilLimitImageToBox();
    });
    this.on("object:scaling", function (e) {
        if (isMobile) {
            var image = self.getActiveObject();
            if (image.scaleX <= self.appRelativeZoom) {
                image.scaleX = self.appRelativeZoom;
                image.scaleY = self.appRelativeZoom;
            }
            ;
            image.center();
        }
        ;
    });
    this.on("selection:cleared", function (e) {
        self._objects.forEach(function (obj) {
            if (obj.type == 'image') {
                self.setActiveObject(obj);
            }
            ;
        });
    });
    this.on("after:render", function (options) {
        if (self.appRender == true) {
            self.utilRenderBorder('render');
            self.appRender = false;
        } else {
            self.utilRenderBorder();
        }
    });
    this.on("mouse:wheel", function (e) {
        var factor = (e.e.deltaY / 100 < 0) ? 1 / 0.985 : 0.985;
        onZoom(factor, e);
        e.e.preventDefault();
        e.e.stopPropagation();
    });


    function onZoom(factor, e) {
        var image = self.getActiveObject();
        if (image != undefined) {
            image.scaleX = image.scaleX * factor;
            image.scaleY = image.scaleY * factor;
            var dx = (Math.round(e.e.x - self._offset.left) - image.left) * (factor - 1),
                dy = (Math.round(e.e.y - self._offset.top) - image.top) * (factor - 1);
            image.left = image.left - dx;
            image.top = image.top - dy;
            self.utilLimitImageToBox();
            self.requestRenderAll();
            if (image.scaleX <= self.appRelativeZoom || image.scaleY <= self.appRelativeZoom) {
                self.utilImageSize();
            }
            ;
        }
        ;
    }

    let domElement = this.contextTop.canvas,
        handler = (domElement.addEventListener || domElement.attachEvent).bind(domElement);

    handler("touchmove", (e) => {
        let firstFinger = this._event.start.touches[0],
            movedFinger = e.touches[0];
        switch (e.touches.length) {
            case 2: {//Pinch
                onZoom(firstFinger.clientX < movedFinger.clientX ? 1 / 0.985 : 0.985, {
                    e: {
                        x: firstFinger.clientX,
                        y: firstFinger.clientY
                    }
                });
                break;
            }
        }
    });
    handler("touchstart", (e) => {
        this._event.start = e;
    })
    this._event = {};
};

editor.Canvas.prototype.setBorderWidth = function (value) {
    this.appBorderWidth = value;
    this.appWidth = this.appRelativeWidth + (this.appBorderWidth * 2);
    this.appHeight = this.appRelativeHeight + (this.appBorderWidth * 2);
    this.utilResize();
    this.utilImageSize();
    this.requestRenderAll();
};

editor.Canvas.prototype.setBorderType = function (value) {
    this.appBorderType = value;
    this.utilImageSize();
    this.requestRenderAll();
};

editor.Canvas.prototype.setScale = function (value) {
    var image = this.getActiveObject();
    var self = this;
    self.utilImageSize();
    image.center();
    var factor = value;
    image.scaleX = image.scaleX * value;
    image.scaleY = image.scaleY * value;
    if (image.scaleX <= self.appRelativeZoom) {
        image.scaleX = self.appRelativeZoom;
        image.scaleY = self.appRelativeZoom;
    }
    ;
    var dx = (Math.round((self.width / 2) - self._offset.left) - image.left) * (factor - 1),
        dy = (Math.round((self.height / 2) - self._offset.top) - image.top) * (factor - 1);
    image.left = image.left - dx;
    image.top = image.top - dy;
    self.utilLimitImageToBox();
    self.requestRenderAll();
};

editor.Canvas.prototype.setFilter = function (value) {
    if (this.getActiveObject() != undefined) {
        var image = this.getActiveObject();
        console.log(editor.Image.filters);
        switch (value) {
            case 'sepia':
                image.filters.push(new editor.Image.filters.Sepia());
                break;
            case 'vintage':
                image.filters.push(new editor.Image.filters.Vintage());
                break;
            case 'technicolor':
                image.filters.push(new editor.Image.filters.Technicolor());
                break;
            case 'blackwhite':
                image.filters.push(new editor.Image.filters.BlackWhite());
                break;
            case 'kodachrome':
                image.filters.push(new editor.Image.filters.Kodachrome());
                break;
            case 'polaroid':
                image.filters.push(new editor.Image.filters.Polaroid());
                break;
            case 'brownie':
                image.filters.push(new editor.Image.filters.Brownie());
                break;
            case 'grayscale':
                image.filters.push(new editor.Image.filters.Grayscale());
                break;
            case 'blur':
                image.filters.push(new editor.Image.filters.Blur(70));
                break;
            case 'pixelate':
                image.filters.push(new editor.Image.filters.Pixelate(70));
                break;
            case 'invert':
                image.filters.push(new editor.Image.filters.Invert());
        }
        ;
    }
    ;
};

editor.Canvas.prototype.applyFilter = function (value) {
    if (this.getActiveObject() != undefined) {
        var image = this.getActiveObject();
        image.applyFilters();
        this.requestRenderAll();
    }
    ;
};

editor.Canvas.prototype.cancelFilter = function (value) {
    if (this.getActiveObject() != undefined) {
        var image = this.getActiveObject();
        image.filters.pop();
        image.applyFilters();
        this.requestRenderAll();
    }
    ;
};

editor.Canvas.prototype.cancelAllFilters = function (value) {
    if (this.getActiveObject() != undefined) {
        var image = this.getActiveObject();
        image.filters = [];
        image.applyFilters();
        this.requestRenderAll();
    }
    ;
};

editor.Canvas.prototype.getDPI = function () {
    var image = this.getActiveObject();
    var dpi = Number((image.width / (image.width * image.scaleX / this.width) / (this.appWidth * 0.393701)).toFixed(1));
    return dpi;
};

editor.Canvas.prototype.getDestDPI = function () {
    var image = this.getActiveObject();
    var dpi = Number((image.width / (this.appWidth * 0.393701)).toFixed(1));
    return dpi;
};

editor.Canvas.prototype.getOptions = function () {
    var image = this.getActiveObject();
    var self = this;
    var object = {
        width: self.appWidth,
        height: self.appHeight,
        borderWidth: self.appBorderWidth,
        borderType: self.appBorderType,
        destDPI: self.getDestDPI(),
        filters: image.filters
    }
    return object;
};

editor.Canvas.prototype.getResult = function () {
    var image = this.getActiveObject();
    var self = this;
    var object = {
        width: Math.round(self.width * (self.getDPI() * self.appWidth) / 2.54 / self.width),
        height: Math.round(self.height * (self.getDPI() * self.appWidth) / 2.54 / self.width),
        realWidth: self.appWidth,
        realHeight: self.appHeight,
        scaleFactor: (self.getDPI() * self.appWidth) / 2.54 / self.width,
        xOffset: -image.left,
        yOffset: -image.top,
        borderWidthPx: self.utilTransformedBorderWidth(),
        filters: image.filters,
        borderWidth: self.appBorderWidth,
        borderType: self.appBorderType,
        dpi: self.getDPI()
    }
    return object;
};

editor.Canvas.prototype.getResultImage = function () {
    this.appRender = true;
    this.requestRenderAll();
    var self = this;
    var result = canvas.toDataURL({
        format: 'jpeg',
        quality: 1,
        multiplier: (self.getDPI() * self.appWidth) / 2.54 / self.width,
        width: self.width,
        height: self.height
    });
    self.requestRenderAll();
    return result;
};

editor.Canvas.prototype.setCenter = function (value) {
    var image = this.getActiveObject();
    image.center();
    this.requestRenderAll();
};

editor.Canvas.prototype.setFitToFrame = function () {
    var image = this.getActiveObject();
    var self = this;
    self.utilImageSize();
    this.requestRenderAll();
};

editor.Canvas.prototype.setFitToStretch = function () {
    var image = this.getActiveObject();
    var self = this;
    self.utilImageSize('stretch');
    this.requestRenderAll();
};

editor.Canvas.prototype.saveJSON = function () {
    var image = this.getActiveObject();
    var self = this;
    console.log(self.getDPI() ,self.appWidth, self.width);
    var object = {
        image: _SETTINGS.image,
        redactorWidth: self.width,
        redactorHeight: self.height,
        appWidth: self.appRelativeWidth,
        appHeight: self.appRelativeHeight,
        appBorderWidth: self.appBorderWidth,
        // appBorderOrWidth: self.appBorderWidth,
        //_a: self.utilTransformedBorderWidth(),
        // multiplier: (self.getDPI() * self.appWidth) / 2.54 / self.width,
        appDPI: self.getDPI(),
        margin: 2.9,// Math.round(2.5 * (this.width / this.appWidth)),
        appBorderType: self.appBorderType,
        text: {value: "canvas24.co.uk", size: _SETTINGS.textSize},
        logo: {source: "Canvas_24_logo_color.png", height: _SETTINGS.logoHeight},
        // appRelativeZoom: self.appRelativeZoom,
        // width: image.width,
        // height: image.height,
        left: image.left,
        top: image.top,
        scaleX: image.scaleX,
        scaleY: image.scaleY,
        filters: image.filters
    }

    return JSON.stringify(object);
};

editor.Canvas.prototype.loadJSON = function (value) {
    var self = this;
    var object = JSON.parse(value);
    self.setCanvas(object.appWidth, object.appHeight, 'cm');
    self.setBorderWidth(object.appBorderWidth);
    self.setBorderType(object.appBorderType);
    var image = self.getActiveObject();

    image.set({
        scaleX: object.scaleX,
        scaleY: object.scaleY,
        left: object.left,
        top: object.top,
        lockRotation: true,
        centeredScaling: true,
        hasBorders: false,
        hasControls: false,
    });
    for (var i = 0; i < object.filters.length; i++) {
        self.setFilter(object.filters[i].type.toLowerCase());
        self.applyFilter();
    }

    //image.filters.push(object.filters);
    //image.applyFilters();
    self.requestRenderAll();

    /*
    self.loadFromJSON(value, function () {
      self.renderAll();
      self.forEachObject(function (obj) {
        if (obj.type == 'image') {
          self.setActiveObject(obj);
          var sx = obj.scaleX;
          var sy = obj.scaleY;
          var sl = obj.left;
          var st = obj.top;
          self.setCanvas(obj.appWidth, obj.appHeight, 'cm');
          self.setBorderWidth(obj.appBorderWidth);
          self.setBorderType(obj.appBorderType);
          obj.set({
            scaleX:sx,
            scaleY:sy,
            left:sl,
            top:st
          });
          self.requestRenderAll();
        }
      });
    });
    */

};

editor.Canvas.prototype.utilRenderBorder = function (state) {
    var self = this;
    var image = self.getActiveObject();
    if (image != undefined && self.utilTransformedBorderWidth() >= 1) {
        // canvas element
        var ctx = document.getElementById(self.lowerCanvasEl.id).getContext("2d");
        // border limit

        //optimizations
        var Rx = 255,
            Gx = 255,
            Bx = 255;
        if (self.appBorderType != 'expandet' && self.appBorderType != 'mirror') {
            Rx = hexToR(self.appBorderType);
            Gx = hexToG(self.appBorderType);
            Bx = hexToB(self.appBorderType);
        }
        ;

        var bLimit = [
            ctx.getImageData(self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth()),
            ctx.getImageData(self.utilTransformedBorderWidth(), self.height - self.utilTransformedBorderWidth() * 2, self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth()),
            ctx.getImageData(self.width - self.utilTransformedBorderWidth() * 2, self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth()),
            ctx.getImageData(self.width - self.utilTransformedBorderWidth() * 2, self.height - self.utilTransformedBorderWidth() * 2, self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth())
        ];
        for (var b = 0; b < bLimit.length; b++) {
            for (var i = 0; i < bLimit[b].data.length; i += 4) {
                if (state == 'render' && (self.appBorderType != 'expandet' && self.appBorderType != 'mirror')) {
                    bLimit[b].data[i + 0] = Rx;
                    bLimit[b].data[i + 1] = Gx;
                    bLimit[b].data[i + 2] = Bx;
                    bLimit[b].data[i + 3] = 255;
                } else if (state == 'render' && self.appBorderType == 'mirror') {

                    //bLimit.data[i + 0] = 203;
                    //bLimit.data[i + 1] = 250;
                    // bLimit.data[i + 2] = 226;
                    bLimit[b].data[i + 3] = 255;
                } else {
                    bLimit[b].data[i + 0] = 255;
                    bLimit[b].data[i + 1] = 255;
                    bLimit[b].data[i + 2] = 255;
                    bLimit[b].data[i + 3] = 1;
                }
                ;
            }
            ;

            if (state == 'render' && self.appBorderType == 'mirror') {
                var output = ctx.createImageData(bLimit[b].width, bLimit[b].height);
                var w = bLimit[b].width;
                var h = bLimit[b].height;
                var dst = output.data;
                var d = bLimit[b].data;
                for (var y = 0; y < h; y++) {
                    for (var x = 0; x < w; x++) {
                        var off = (y * w + x) * 4;
                        var dstOff = (y * w + (w - x - 1)) * 4;
                        dst[dstOff] = d[off];
                        dst[dstOff + 1] = d[off + 1];
                        dst[dstOff + 2] = d[off + 2];
                        dst[dstOff + 3] = d[off + 3];
                    }
                }
                ;
                bLimit[b] = output;

                var output1 = ctx.createImageData(bLimit[b].width, bLimit[b].height);
                var w1 = bLimit[b].width;
                var h1 = bLimit[b].height;
                var dst1 = output1.data;
                var d1 = bLimit[b].data;
                for (var y = 0; y < h1; y++) {
                    for (var x = 0; x < w1; x++) {
                        var off = (y * w1 + x) * 4;
                        var dstOff = ((h1 - y - 1) * w1 + x) * 4;
                        dst1[dstOff] = d1[off];
                        dst1[dstOff + 1] = d1[off + 1];
                        dst1[dstOff + 2] = d1[off + 2];
                        dst1[dstOff + 3] = d1[off + 3];
                    }
                }
                ;
                bLimit[b] = output1;
            }

            if (state == 'render' && self.appBorderType == 'expandet') {

            } else {
                // top-left
                if (b == 0) {
                    ctx.putImageData(bLimit[b], 0, 0);
                } else if (b == 1) {
                    // bottom-left
                    ctx.putImageData(bLimit[b], 0, self.height - self.utilTransformedBorderWidth());
                } else if (b == 2) {
                    // top-right
                    ctx.putImageData(bLimit[b], self.width - self.utilTransformedBorderWidth(), 0);
                } else if (b == 3) {
                    // bottom-right
                    ctx.putImageData(bLimit[b], self.width - self.utilTransformedBorderWidth(), self.height - self.utilTransformedBorderWidth());
                }
                ;
            }
            ;
        }
        ;

        switch (self.appBorderType) {
            case 'expandet':
                var canvasData = [
                    ctx.getImageData(0, self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2)),
                    ctx.getImageData(self.width - self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2)),
                    ctx.getImageData(self.utilTransformedBorderWidth(), 0, self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth()),
                    ctx.getImageData(self.utilTransformedBorderWidth(), self.height - self.utilTransformedBorderWidth(), self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth())
                ];
                for (var i = 0; i < canvasData.length; i++) {
                    // alpha color
                    for (var j = 3; j < canvasData[i].data.length; j += 4) {
                        if (state == 'render') {
                            canvasData[i].data[j] = 255;
                        } else {
                            canvasData[i].data[j] = 155;
                        }
                        ;
                    }
                    ;
                    // render
                    if (i == 0) {
                        ctx.putImageData(canvasData[i], 0, self.utilTransformedBorderWidth());
                    } else if (i == 1) {
                        ctx.putImageData(canvasData[i], self.width - self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth());
                    } else if (i == 2) {
                        ctx.putImageData(canvasData[i], self.utilTransformedBorderWidth(), 0);
                    } else if (i == 3) {
                        ctx.putImageData(canvasData[i], self.utilTransformedBorderWidth(), self.height - self.utilTransformedBorderWidth());
                    }
                    ;
                }
                ;
                break;
            case 'mirror':
                var canvasData = [
                    ctx.getImageData(self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2)),
                    ctx.getImageData(self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2)),
                    ctx.getImageData(self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth(), self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth()),
                    ctx.getImageData(self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2), self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth())
                ];
                for (var i = 0; i < canvasData.length; i++) {
                    // alpha color
                    for (var j = 3; j < canvasData[i].data.length; j += 4) {
                        if (state == 'render') {
                            canvasData[i].data[j] = 255;
                        } else {
                            canvasData[i].data[j] = 155;
                        }
                    }
                    ;
                    if (i < 2) {
                        var output = ctx.createImageData(canvasData[i].width, canvasData[i].height);
                        var w = canvasData[i].width;
                        var h = canvasData[i].height;
                        var dst = output.data;
                        var d = canvasData[i].data;
                        for (var y = 0; y < h; y++) {
                            for (var x = 0; x < w; x++) {
                                var off = (y * w + x) * 4;
                                var dstOff = (y * w + (w - x - 1)) * 4;
                                dst[dstOff] = d[off];
                                dst[dstOff + 1] = d[off + 1];
                                dst[dstOff + 2] = d[off + 2];
                                dst[dstOff + 3] = d[off + 3];
                            }
                        }
                        ;
                        canvasData[i] = output;

                    } else if (i > 1) {
                        var output = ctx.createImageData(canvasData[i].width, canvasData[i].height);
                        var w = canvasData[i].width;
                        var h = canvasData[i].height;
                        var dst = output.data;
                        var d = canvasData[i].data;
                        for (var y = 0; y < h; y++) {
                            for (var x = 0; x < w; x++) {
                                var off = (y * w + x) * 4;
                                var dstOff = ((h - y - 1) * w + x) * 4;
                                dst[dstOff] = d[off];
                                dst[dstOff + 1] = d[off + 1];
                                dst[dstOff + 2] = d[off + 2];
                                dst[dstOff + 3] = d[off + 3];
                            }
                        }
                        ;
                        canvasData[i] = output;
                    }
                    ;
                    // render
                    if (i == 0) {
                        ctx.putImageData(canvasData[i], 0, self.utilTransformedBorderWidth());
                    } else if (i == 1) {
                        ctx.putImageData(canvasData[i], self.width - self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth());
                    } else if (i == 2) {
                        ctx.putImageData(canvasData[i], self.utilTransformedBorderWidth(), 0);
                    } else if (i == 3) {
                        ctx.putImageData(canvasData[i], self.utilTransformedBorderWidth(), self.height - self.utilTransformedBorderWidth());
                    }
                    ;
                }
                ;
                break;
            default:
                var canvasData = [
                    ctx.createImageData(self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2)),
                    ctx.createImageData(self.utilTransformedBorderWidth(), self.height - (self.utilTransformedBorderWidth() * 2)),
                    ctx.createImageData(self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth()),
                    ctx.createImageData(self.width - (self.utilTransformedBorderWidth() * 2), self.utilTransformedBorderWidth())
                ];
                for (var i = 0; i < canvasData.length; i++) {
                    for (var j = 0; j < canvasData[i].data.length; j += 4) {
                        canvasData[i].data[j + 0] = Rx;
                        canvasData[i].data[j + 1] = Gx;
                        canvasData[i].data[j + 2] = Bx;
                        canvasData[i].data[j + 3] = 255;
                    }
                    // render
                    if (i == 0) {
                        ctx.putImageData(canvasData[i], 0, self.utilTransformedBorderWidth());
                    } else if (i == 1) {
                        ctx.putImageData(canvasData[i], self.width - self.utilTransformedBorderWidth(), self.utilTransformedBorderWidth());
                    } else if (i == 2) {
                        ctx.putImageData(canvasData[i], self.utilTransformedBorderWidth(), 0);
                    } else if (i == 3) {
                        ctx.putImageData(canvasData[i], self.utilTransformedBorderWidth(), self.height - self.utilTransformedBorderWidth());
                    }
                    ;
                }
                ;
        }
        ;
    }
    ;
};

editor.Canvas.prototype.utilResize = function () {
    var canvasContainer = document.getElementById(this.appParentContainer);
    var ratio = this.appWidth / this.appHeight;
    if (ratio < 1) {
        this.setWidth(canvasContainer.offsetWidth)
        this.setHeight(Math.round(this.width / ratio));
    } else if (ratio == 1) {
        this.setWidth(canvasContainer.offsetWidth);
        this.setHeight(this.width);
    } else if (ratio > 1) {
        this.setWidth(canvasContainer.offsetWidth);
        this.setHeight(Math.round(this.width / ratio));
    }
    ;
    if (this.getActiveObject() != undefined) {
        this.utilImageSize();
    }
    ;
    this.calcOffset();
    this.requestRenderAll();
};

editor.Canvas.prototype.utilImageSize = function (exception) {
    var image = this.getActiveObject();
    var self = this;
    if (self.appBorderType != 'expandet') {
        var mnf_self_width = self.width - (self.utilTransformedBorderWidth() * 2) + 10;
        var mnf_self_height = self.height - (self.utilTransformedBorderWidth() * 2) + 10;
        if (exception != 'stretch') {
            image.scaleToWidth(mnf_self_width);
        } else {
            image.scaleToWidth(mnf_self_width);
            image.set({
                scaleY: mnf_self_height / image.height
            })
        }
        if (image.getBoundingRect().height < mnf_self_height) {
            if (exception != 'stretch') {
                image.scaleToHeight(mnf_self_height);
            } else {
                image.scaleToHeight(mnf_self_height);
                image.set({
                    scaleX: mnf_self_width / image.width
                })
            }
        }
        ;
    } else {
        if (exception != 'stretch') {
            image.scaleToWidth(this.width);
            if (image.getBoundingRect().height < this.height) {
                image.scaleToHeight(this.height);
            }
            ;
        } else {
            image.scaleToWidth(this.width);
            image.set({
                scaleY: this.height / image.height
            })
        }
    }
    ;
    image.center();
    self.appRelativeZoom = image.scaleX;
    this.requestRenderAll();
};

editor.Canvas.prototype.utilLimitImageToBox = function () {
    var image = this.getActiveObject();
    image.setCoords();
    var self = this;
    if (self.appBorderType == 'expandet') {
        if (image.getBoundingRect().left >= 0) {
            image.left = 0;
        }
        ;
        if (image.getBoundingRect().left + image.getBoundingRect().width <= this.width) {
            image.left = -image.getBoundingRect().width + this.width
        }
        ;
        if (image.getBoundingRect().top >= 0) {
            image.top = 0;
        }
        ;
        if (image.getBoundingRect().top + image.getBoundingRect().height <= this.height) {
            image.top = -image.getBoundingRect().height + this.height
        }
        ;
    } else {
        if (image.getBoundingRect().left >= self.utilTransformedBorderWidth() - 5) {
            image.left = self.utilTransformedBorderWidth() - 5;
        }
        ;
        if (image.getBoundingRect().left + image.getBoundingRect().width <= this.width - self.utilTransformedBorderWidth() + 5) {
            image.left = -image.getBoundingRect().width + this.width - self.utilTransformedBorderWidth() + 5
        }
        ;
        if (image.getBoundingRect().top >= self.utilTransformedBorderWidth() - 5) {
            image.top = self.utilTransformedBorderWidth() - 5;
        }
        ;
        if (image.getBoundingRect().top + image.getBoundingRect().height <= this.height - self.utilTransformedBorderWidth() + 5) {
            image.top = -image.getBoundingRect().height + this.height - self.utilTransformedBorderWidth() + 5
        }
        ;
    }
    ;
};

editor.Canvas.prototype.utilTransformedBorderWidth = function () {
    return Math.round(this.appBorderWidth * (this.width / this.appWidth));
};

// filter backend
editor.filterBackend = new editor.Canvas2dFilterBackend();

module.exports=editor;