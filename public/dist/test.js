
(function () {

    var canvas = window.__Canvas = new fabric.Canvas("c", {
        appParentContainer: 'canvasContainer'
    });


    canvas.startEvents();
    canvas.on("after:render", function (options) {
        if (canvas.getActiveObject() != undefined) {
            console.log('DPI: ', canvas.getDPI());
        }
        ;
    });

    canvas.setCanvas(60, 40, 'cm');

    // UI
    document.getElementById('file').addEventListener("change", function (e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.onload = function (f) {
            var data = f.target.result;
            canvas.setImage(data);
        };
        reader.readAsDataURL(file);
        var formData = new FormData();
        formData.append("image", file);
        var http = new XMLHttpRequest();
        http.open('POST', canvas.__config._URL + "api/image", true);
        // http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.onreadystatechange = function () {
            if (http.readyState == 4 && http.status == 200) {
                canvas.__config._SETTINGS.image = http.responseText;
            }
        }
        http.send(formData);
    });
    



    var setcanvaswidth_select =  document.getElementById('setcanvaswidth');
    var setcanvasheight_select =  document.getElementById('setcanvasheight');
    setcanvaswidth_select.addEventListener("change", function (e) {
        canvas.setCanvas(e.target.value, setcanvasheight_select.options[setcanvasheight_select.selectedIndex].value, 'cm');

    });
    setcanvasheight_select.addEventListener("change", function (e) {
        canvas.setCanvas(setcanvaswidth_select.options[setcanvaswidth_select.selectedIndex].value,e.target.value, 'cm');
    });


    document.getElementById('borderwidth').addEventListener("change", function (e) {
        canvas.setBorderWidth(Number(e.target.value));
    });
    document.getElementById('bordertype').addEventListener("change", function (e) {
        canvas.setBorderType(e.target.value);
    });
    document.getElementById('zoom').addEventListener("click", function (e) {
        canvas.setScale(randomNumb(3, 0.7));
    });
    document.getElementById('setfilter').addEventListener("change", function (e) {
        canvas.setFilter(e.target.value);
    });
    document.getElementById('applyfilter').addEventListener("click", function (e) {
        canvas.applyFilter();
    });
    document.getElementById('cancelfilter').addEventListener("click", function (e) {
        canvas.cancelFilter();
    });
    document.getElementById('setimageurl').addEventListener("click", function (e) {
        canvas.setImage('image.jpg');
    });
    document.getElementById('cancelallfilters').addEventListener("click", function (e) {
        canvas.cancelAllFilters();
    });
    document.getElementById('getDPI').addEventListener("click", function (e) {
        console.log(canvas.getDPI());
    });
    document.getElementById('getDestDPI').addEventListener("click", function (e) {
        console.log(canvas.getDestDPI());
    });
    document.getElementById('getOptions').addEventListener("click", function (e) {
        console.log(canvas.getOptions());
    });
    document.getElementById('getResult').addEventListener("click", function (e) {
        console.log(canvas.getResult());
    });
    document.getElementById('getResultImage').addEventListener("click", function (e) {
        //console.log(getResultImage());
    });
    document.getElementById('setCenter').addEventListener("click", function (e) {
        canvas.setCenter();
    });
    document.getElementById('setFitToFrame').addEventListener("click", function (e) {
        canvas.setFitToFrame();
    });
    document.getElementById('setFitToStretch').addEventListener("click", function (e) {
        canvas.setFitToStretch();
    });
    document.getElementById('loadjsonbtn').addEventListener("click", function (e) {
        canvas.loadJSON(document.getElementById('loadsavejson').value);
    });
    document.getElementById('savejsonbtn').addEventListener("click", function (e) {
        let textArea = document.getElementById('loadsavejson');
        textArea.value = canvas.saveJSON();

    });

    // for testing output

    document.getElementById('reqsonbtn').addEventListener("click", function (e) {
        try{
            var value = JSON.stringify(JSON.parse(document.getElementById('loadsavejson').value));
            

            //console.log( JSON.parse(document.getElementById('loadsavejson').value) );


            var http = new XMLHttpRequest();
            var url = canvas.__config._URL + "api/";
            http.open('POST', url, true);
            // http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            http.onreadystatechange = function () {
                if (http.readyState == 4 && http.status == 200) {
                    var img = document.createElement("img"),
                        resp = http.responseText;
                    try {
                        resp = JSON.parse(resp);
                        resp = resp.path;
                    } catch (e) {

                    }
                    img.src = canvas.__config._URL + resp;
                    document.body.appendChild(img);
                }
            }
            http.setRequestHeader("Content-Type", "application/json");
            http.send(value);

            /*var params = 'image=' + _SETTINGS.image + '&' +
                'appWidth=' + self.width + '&' +
                'appHeight=' + self.height + '&' +
                'outWidth=' + self.appRelativeWidth + '&' +
                'outHeight=' + self.appRelativeHeight + '&' +
                'outBorderWidth=' + self.appBorderWidth + '&' +
                'outBorderType=' + self.appBorderType + '&' +
                'scaleX=' + image.scaleX + '&' +
                'scaleY=' + image.scaleY + '&' +
                'left=' + image.left + '&' +
                'top=' + image.top + '&' +
                'filters=' + JSON.stringify(image.filters) + '&' +
                'getImage=&' +
                'saveImage=&' +
                'margin=' + 2.5 + '&' +
                'logo=&' +
                'text=';

            // http.send(params);*/
        }catch(e){
            alert("Incorrect JSON requested")
        }

    });
    document.getElementById('download').addEventListener("click", function (e) {
        saveImage();
    });

    function saveImage() {
        canvas.appRender = true;
        canvas.requestRenderAll();
        var link = canvas.toDataURL({
            format: 'jpeg',
            quality: 1,
            multiplier: (canvas.getDPI() * canvas.appWidth) / 2.54 / canvas.width,
            width: canvas.width,
            height: canvas.height
        });
        canvas.requestRenderAll();

        var img_dom = document.createElement('img');
        img_dom.src = link;
        document.body.appendChild(img_dom);
    }


})();